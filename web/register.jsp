<%--
  Created by IntelliJ IDEA.
  User: himedere
  Date: 11/05/2018
  Time: 10:22
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%
        Cookie listCookie[] = request.getCookies();
        String account = "";
        String password = "";
        String email;
        account = listCookie[0].getValue();
        password = listCookie[0].getValue();
        email = listCookie[0].getValue();
    %>
    <title>Create new User</title>
    <style>
        .message{
            color: forestgreen;
        }
    </style>
</head>
<body>
<h1>Welcome new User to GymMP3</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>
<form method="post">
    <fieldset>
        <legend>Enter your information below:</legend>
        <table>
            <tr>
                <td>Username: </td>
                <td><input type="text" name="account" placeholder="Enter your username" value="<%out.print(account);%>"></td>
            </tr>
            <tr>
                <td>Password: </td>
                <td><input type="password" name="password" placeholder="Enter your password" value="<%out.print(password);%>" ></td>
            </tr>
            <tr>
                <td>First Name:</td>
                <td><input type="text" name="first_name" placeholder="Enter your First name"></td>
            </tr>
            <tr>
                <td>Last Name: </td>
                <td><input type="text" name="last_name" placeholder="Enter your Last name"></td>
            </tr>
            <tr>
                <td>Birthday: </td>
                <td><input type="date" name="birthday" placeholder="Enter your birthday"></td>
            </tr>
            <tr>
                <td>Avatar Image URL: </td>
                <td><input type="url" name="image" placeholder="Copy your avatar URL here"></td>
            </tr>
            <tr>
                <td>Gender: </td>
                <td>
                    <select name="Gender">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other gender</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Email: </td>
                <td><input type="email" name="email" placeholder="Enter your email" value="<%out.print(email);%>"></td>
            </tr>
            <tr>
                <td>Phone Number:</td>
                <td><input type="text" name="phone" placeholder="Enter your phone number"></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Register"/></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
