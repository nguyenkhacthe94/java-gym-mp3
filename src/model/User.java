package model;

import java.util.Date;

public class User {
    private int id;
    private String account;
    private String first_name;
    private String last_name;
    private String password;
    private String image;
    private String email;
    private int gender;
    private Date birthday;
    private String phone;
    private Date created_date;

    public User() {
    }

    public int getId() {
        return id;
    }

    public String getAccount() {
        return account;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPassword() {
        return password;
    }

    public String getImage() {
        return image;
    }

    public String getEmail() {
        return email;
    }

    public int getGender() {
        return gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getPhone() {
        return phone;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }
}
