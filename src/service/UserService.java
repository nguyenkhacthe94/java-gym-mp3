package service;

import java.sql.SQLException;
import java.util.List;
import model.User;

public interface UserService {

    List<User> findAll() throws ClassNotFoundException, SQLException;
}
