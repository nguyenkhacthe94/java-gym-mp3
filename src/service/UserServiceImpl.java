package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.User;

public class UserServiceImpl implements UserService {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/gym_mp3";
    static final String DB_USERNAME = "root";
    static final String DB_PASSWORD = "aptx4869";

    public UserServiceImpl () {}

    public List<User> findAll() throws SQLException, ClassNotFoundException {
        Class.forName(JDBC_DRIVER);
        Connection connection = null;
        Statement statement = null;

        //Connecting to Database...;
        connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);

       //Creating Database query...
        statement = connection.createStatement();
        String sql;
        sql = "select * from user";

        //Query is in process...
        ResultSet resultSet = statement.executeQuery(sql);

        List<User> users = new ArrayList<>();
        //Collecting result...
            User user = new User();
            if (user.getAccount().equals(resultSet.getString("account"))){
            user.setId(resultSet.getInt("id"));
            user.setAccount(resultSet.getString("account"));
            user.setPassword(resultSet.getString("password"));
            user.setFirst_name(resultSet.getString("first_name"));
            user.setLast_name(resultSet.getString("last_name"));
            user.setImage(resultSet.getString("image"));
            user.setEmail(resultSet.getString("Email"));
            user.setGender(resultSet.getInt("gender"));
            user.setBirthday(resultSet.getDate("birthday"));
            user.setPhone(resultSet.getString("phone"));
            user.setCreated_date(resultSet.getDate("created_date"));

            users.add(user);
            //Collecting result is done. Closing connects...
            resultSet.close();
            statement.close();
            connection.close();
        }
        return users;
    }
}